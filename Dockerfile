FROM node:18.12.0 AS build
COPY . /frontend
WORKDIR /frontend
RUN npm install --registry=http://registry.npm.taobao.org
RUN npm run build

FROM nginx
COPY --from=build /frontend/dist/ /usr/share/nginx/html/
