import { createPinia } from "pinia";
import { createApp, defineAsyncComponent } from "vue";
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import router from './router/index';
import App from './App.vue';
import './styles/reset.scss'; // 全局属性
import './styles/globa.scss';
import './styles/animateIn.scss'; // 全局属性 进入动画
import './styles/animateOut.scss'; // 全局属性 消失动画

// 自定义组件
const components = ["VText"];

const app = createApp(App);

// 按需加载自定义组件
components.forEach((key) => {
  app.component(
    key,
    defineAsyncComponent(() => import(`./custom-component/${key}/Component.vue`))
  );
  app.component(
    key + 'Attr',
    defineAsyncComponent(() => import(`./custom-component/${key}/Attr.vue`))
  );
});

app.use(ElementPlus);
app.use(router);
app.use(createPinia());

app.mount("#app");
