export const data = {
  blocks: [
    {
      component: "VText",
      label: "文本",
      propValue: "双击编辑文字",
      icon: "wenben",
      animations: [],
      events: {},
      request: {
        method: "GET",
        data: [],
        url: "",
        series: false,
        time: 1000,
        paramType: "",
        requestCount: 0,
      },
      style: {
        width: 200,
        height: 28,
        fontSize: 20,
        fontWeight: 400,
        lineHeight: "",
        letterSpacing: 0,
        textAlign: "",
        color: "",
      },
    },
  ],
};
